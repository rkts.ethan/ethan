import matplotlib.pyplot as plt
from random import *
class Monde:
    def __init__(self,dimension):
        self.dimension=dimension
        self.duree=30
        self.carte=[[0 for i in range(dimension)]for j in range(dimension)]
        for i in range(dimension*dimension//2):#on prend la moitié des carrés et on y met de l'herbe
            self.carte[randint(0,dimension-1)][randint(0,dimension-1)]=randint(30,100)
        for j in range(dimension): #on met des carrés non herbus pour le reste
            for i in range(dimension):
                if self.carte[j][i]==0:
                    self.carte[j][i]=randint(0,30)
    def herbePousse(self):
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.carte[i][j]+=1
    def herbeMangee(self,i,j):
        self.carte[i][j]=0
    def nbHerbe(self):
        cmpt=0
        for i in self.carte:
            for j in i:
                if j>=self.duree:
                    cmpt=cmpt+1
        return cmpt
    def getCoefCarte(self, i, j):
        return self.carte[i][j]
    
class Mouton:
    
    def __init__(self,position_x,position_y,gain_nourriture,taux_reproduction):
        
        self.x=position_x
        self.y=position_y
        self.energie=randint(gain_nourriture,gain_nourriture*2)
        self.gain=gain_nourriture
        self.taux=taux_reproduction
        
    def variationEnergie(self,monde):
        if monde.getCoefCarte(self.x,self.y)>=monde.duree : #vérifie si c'est un carré herbu
            self.energie+=self.gain
            monde.herbeMangee(self.x,self.y) #le mouton a mangé l'herbe donc on met le coefficient du carré à 0.  
        else:  
            self.energie-=1
        
    def deplacement(self,monde):
        a=[self.x-1,self.x+1,self.x]
        b=[self.y-1,self.y+1,self.y]
        self.x=choice(a) #choisit une des 8 cases adjacentes en sachant qu'il peut ne pas bouger
        self.y=choice(b)
        self.x=(self.x+monde.dimension)%monde.dimension #formule pour un monde torique, si le mouton dépasse les cases il reste dans le monde
        self.y=(self.y+monde.dimension)%monde.dimension
        
    def place_mouton(self):
        return Mouton(self.x,self.y,self.gain,self.taux) #retourne un mouton sur la même case
    
class Simulation:
    
    def __init__(self,monde,fin_du_monde, nbmoutons,gainmouton,tauxmouton,nbfin):
        self.monde=monde
        self.horloge=0
        self.fin=fin_du_monde
        self.moutons=[]
        for i in range(nbmoutons):
            mouton=Mouton(randint(0,monde.dimension-1),randint(0,monde.dimension-1),gainmouton) #création des moutons
            self.moutons.append(mouton)

        self.taux=round(tauxmouton*(100/len(self.moutons)))
        self.nbfin=nbfin
        
        self.resultats_herbe=[self.monde.nbHerbe()]
        self.resultats_moutons=[len(self.moutons)]
        

    def reproduction(self):
        for i in range(self.taux):
            indice=randint(0,len(self.moutons)-1) #on prend un mouton au hasard
            self.moutons.append(self.moutons[indice].place_mouton()) #on ajoute un enfant
        
    def variation(self):
        for i in range(len(self.moutons)):
            self.moutons[i].variationEnergie(self.monde) #on varie l'éenrgie de tous les moutons
        for i in self.moutons:
            if i.energie<=0:
                self.moutons.remove(i) #supprimer les moutons qui n'ont plus d'énergie
    def simMouton(self):
        while self.horloge<self.fin and len(self.moutons)>self.nbfin:
            self.monde.herbePousse()
            self.reproduction()
            for i in range(len(self.moutons)): #les moutons se déplacent
                self.moutons[i].deplacement(self.monde)
            self.variation()
            self.horloge+=1
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.resultats_moutons.append(len(self.moutons))
            print(self.horloge)
        x = [i for i in range(self.fin+1)]
        plt.plot(x,self.resultats_herbe,label="Herbe")
        plt.plot(x,self.resultats_moutons,label="Moutons")
        plt.legend()
        plt.show()

            
                

        



    



